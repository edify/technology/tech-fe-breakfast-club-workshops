# Breakfast Club Workshop: Headless Component Pattern - Separation of concerns

[Session Recording](https://drive.google.com/file/d/1Q6Exj8TmwhF1QJLjyf9b5cxn7ZidOfw8/view?usp=sharing)

Loosely based on:
1. [Headless Component: a pattern for composing React UIs](https://martinfowler.com/articles/headless-component.html)
2. [Headless UI Components: Creating re-usable logic without thinking about design](https://medium.com/cstech/headless-ui-components-creating-re-usable-logic-without-thinking-about-design-69ac9fad6400)
3. [Headless Components](https://blog.alyssaholland.me/headless-components#heading-what-are-headless-components)
4. [Headless components in React and why I stopped using a UI library for our design system](https://medium.com/@nirbenyair/headless-components-in-react-and-why-i-stopped-using-ui-libraries-a8208197c268)
5. [Decoupling UI and Logic in React: A Clean Code Approach with Headless Components](https://itnext.io/decoupling-ui-and-logic-in-react-a-clean-code-approach-with-headless-components-82e46b5820c)
6. [The complete guide to building headless interface components in React](https://blog.logrocket.com/the-complete-guide-to-building-headless-interface-components-in-react/)

## What is Headless Components Pattern?

A [Headless Component is a design pattern in React](https://martinfowler.com/articles/headless-component.html#ImplementingHeadlessComponentWithACustomHook) where a component - normally implemented as React hooks - is responsible solely for logic and state management without prescribing any specific UI (User Interface). It provides the “brains” of the operation but leaves the “looks” to the developer implementing it. In essence, it offers functionality without forcing a particular visual representation.

![Headless Component Pattern from MartinFowler.com](public/headless-component.png)

[Headless components](https://medium.com/cstech/headless-ui-components-creating-re-usable-logic-without-thinking-about-design-69ac9fad6400) are the components that have no user interface but have functionality. It does not care about how your components look, how they are designed, styled. It just gives you the functionality and how it will look when it is mounted or hovered is under your control. It separates the logic and behavior of a component from its visual representation.

Headless components are [unopinionated, unstyled, components](https://blog.alyssaholland.me/headless-components#heading-what-are-headless-components) that handle a majority of the tricky implementation details so that you can build out components faster. Headless components do not have preset styles but instead provide full, interactive functionality for a particular component pattern.

In React, headless components are [usually implemented as custom hooks](https://martinfowler.com/articles/headless-component.html#ImplementingHeadlessComponentWithACustomHook). In Angular, we can also create headless components, but this pattern [is not that popular within the Angular community](https://dev.to/haydenbr/headless-angular-components-nm2). The [Composition API in Vue.js](https://dev.to/jesusantguerrero/from-my-point-of-vue-headless-components-2403) allows to create headless components.

### Headless UI components

Headless Component Pattern is widely used to create [headless user interface components libraries](https://medium.com/cstech/headless-ui-components-creating-re-usable-logic-without-thinking-about-design-69ac9fad6400). These kind of component libraries that offers maximum visual flexibility by providing no interface.

### Benefits

1. **Async Data Considerations:** As your application scales, manage loading, error, and empty states within the headless component.
1. **Built-in Accessibility:** Creating [accessible components](https://react-spectrum.adobe.com/react-aria/index.html) that meet the WAI-ARIA standards and properly handle aspects like keyboard navigation can be a large undertaking. Headless component libraries aim to simplify this often difficult process by providing a set of flexible and extensible components that are prebuilt with accessibility and developer experience in mind.
1. **Reusable & Flexible:** Headless components are typically flexible enough to be able to handle most business requirements and implementation details. It should be easy and seamless to modify and maintain. Over time, you might need additional features to an already complex component.
1. **Inversion of Control:** Headless components put a majority of the power in the developer's hands via "inversion of control". This pattern obscures enough of the implementation details and provides you with an API that you can control and style to meet most needs.
1. **UI Variations and Theming:** Different parts of your application might require different styles or themes for the dropdown. Managing these variations within the component can lead to an explosion of props and configurations.
1. **Browser support:** it should support all major browsers.
1. **Responsiveness:** it should support all screen sizes and devices.

### When to use Headless components?
Headless components are also useful when you’re building the same functionality with different UI in your application. For example, headless components are good for dropdown components, table components, and tabs components.

### When headless components are overkill?
If you don’t have multiple UI for the same functionality in your application, or if you are not building a reusable component library for others to use, then headless components may not be necessary.


## Hands-on exercise

Fix the [ExampleWithoutSeparationOfConcerns.tsx](https://gitlab.com/edify/technology/tech-fe-breakfast-club-workshops/-/blob/main/2024-01-10_workshop-headless-components-separation-of-concerns/src/BasicExampleWithoutSeparationOfConcerns.tsx) implementation by adding separation of concerns by implementing a headless component. There're other things in that component that can and should be fixed, so feel free to change everything you think will improve the code: add/remove components, use `children` prop, create contexts, etc.


### In order to run the app
 ```
 npm install
 npm run dev
 ```

## Cool things to do

* **Provided example was too easy?** Ask ChatGPT or Bard to generate a complex example were you can separate logic from styling using a headless component.

* Do you think you have **something in your project that could be fixed** with Headless Component Pattern? Let's check it out and maybe together we can improve your project.

* Why only Javascript/Typescript and React? Do you think this pattern can be applied in another programming language, framework or technology? **Create a merge request and add your own example**.

## Advanced topics and examples

* [Headless Component: a pattern for composing React UIs](https://martinfowler.com/articles/headless-component.html)
* [Build your own headless UI components - Jacob Paris](https://www.jacobparis.com/content/react-headless-submenus)
* [Awesome React Headless Componets](https://github.com/jxom/awesome-react-headless-components)
* [Headless components in React and why I stopped using a UI library for our design system](https://medium.com/@nirbenyair/headless-components-in-react-and-why-i-stopped-using-ui-libraries-a8208197c268)
