import { useState } from 'react';

/**
 * Counter application
 * Step-by-step guide on how to create and use a headless component to achieve this separation of concerns
 * on a counter application
 *
 * Step 1: Create the Headless Component logic:
 * - Count state
 * - Increment and decrement modifying functions
 * Step 2: Create the Presentation Component
 * Step 3: Use the Headless Component in Your App
 * */

const Counter = (initialCount = 0) => {
  const [count, setCount] = useState(initialCount);

  const increment = () => setCount(count + 1);
  const decrement = () => setCount(count - 1);

  return (
    <div>
      <p>Count: {count}</p>
      <button onClick={increment}>Increment</button>
      <button onClick={decrement}>Decrement</button>
    </div>
  );
};

export default Counter;
