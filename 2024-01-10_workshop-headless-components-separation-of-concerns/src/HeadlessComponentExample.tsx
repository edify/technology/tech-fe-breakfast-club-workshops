import React, {useCallback, useState} from 'react';
import {Button, Label, ListBox, ListBoxItem, Popover, Select, SelectValue} from 'react-aria-components';

// This component just sets the current theme of an application and displays what the current theme is.
export const Theme1 = () => {
  const [theme, setTheme] = useState('light');

  return (
    <div>
      <button onClick={() => setTheme('light')}>Set as Light</button>
      <button onClick={() => setTheme('dark')}>Set as Dark</button>
      <span>Current theme 1 is: {theme}</span>
    </div>
  );
}

export const Theme2 = (props) => {
  const {lightIcon, darkIcon} = props;
  const [theme, setTheme] = useState('light');

  return (
    <div>
      <button onClick={() => setTheme('light')}>
        {lightIcon ? lightIcon :  'Set as Light'}
      </button>
      <button onClick={() => setTheme('dark')}>
        {darkIcon ? darkIcon :  'Set as Dark'}
      </button>
      <span>Current theme 2 is: {theme}</span>
    </div>
  );
}

export const Theme3 = (props) => {
  const {lightIcon, darkIcon, showCurrentTheme = true} = props;
  const [theme, setTheme] = useState('light');

  return (
    <div>
      <button onClick={() => setTheme('light')}>
        {lightIcon ? lightIcon :  'Set as Light'}
      </button>
      <button onClick={() => setTheme('dark')}>
        {darkIcon ? darkIcon :  'Set as Dark'}
      </button>
      {showCurrentTheme ? <span>Current theme 3 is: {theme}</span> : null}
    </div>
  );
}

type ThemeTypes = 'light' | 'dark';

interface IuseTheme {
  defaultTheme: ThemeTypes;
}

export const useTheme = ({ defaultTheme = 'light' }: IuseTheme) => {
  const [theme, setTheme] = useState(defaultTheme);

  const setDark = () => setTheme('dark');
  const setLight = () => setTheme('light');

  return {
    theme,
    setDark,
    setLight,
  }
}

export const UsingHeadlessComponent = () => {
  const { theme, setDark, setLight } = useTheme({ defaultTheme: 'light'});

  const onSelectChange = useCallback((value: any) => {
    switch (value as ThemeTypes) {
      case 'dark':
        setDark();
        break;
      default:
        setLight()
    }
  }, []);

  return (
    <>
      <p>Current theme is {theme}</p>
      <button onClick={setDark}>Set as Dark</button>
      <p role="button" style={{ background: 'red', display: 'inline-block', padding: 5, color: 'white'}} onClick={setLight}>Set as Light</p>
      <button onClick={setLight}>💡</button>
      <Select id="theme" onSelectionChange={onSelectChange} selectedKey={theme}>
        <Label>Theme</Label>
        <Button>
          <SelectValue />
          <span aria-hidden="true">▼</span>
        </Button>
        <Popover>
          <ListBox>
            <ListBoxItem id='light'>Light</ListBoxItem>
            <ListBoxItem id='dark'>Dark</ListBoxItem>
          </ListBox>
        </Popover>
      </Select>
    </>
  )
}
