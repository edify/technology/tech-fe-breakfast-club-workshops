import React from 'react';

import './App.css'
import ExampleWithPropDrilling from "./BasicExampleWithoutSeparationOfConcerns.tsx";
import {
  UsingHeadlessComponent,
  Theme1,
  Theme2,
  Theme3
} from "./HeadlessComponentExample.tsx";

function App() {

  return (
    <>
      <h1>The Breakfast Club: Headless Component Pattern to enhance separation of concerns </h1>
      <div>
        <h2>Examples without headless components</h2>
        <Theme1 />
        <Theme2 />
        <Theme3 />
      </div>

      <div>
        <h2>Examples With headless components</h2>
        <UsingHeadlessComponent/>
      </div>

    </>
  )
}

export default App
