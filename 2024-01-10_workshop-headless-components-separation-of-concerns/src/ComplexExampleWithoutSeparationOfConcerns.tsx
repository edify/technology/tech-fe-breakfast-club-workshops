import React, { useState, useEffect } from 'react';

/**
 * Let's consider a more complex example involving data fetching and rendering.
 * In this scenario, a component is responsible for both fetching data from an API and rendering the UI.
 * This violates the separation of concerns, making the component harder to maintain and less reusable.
 *
 * Step 1: Create the Headless Component logic:
 * Step 2: Create the Presentation Component
 * Step 3: Use the Headless Component in Your App
 * */


interface IUser {
  id: string;
  name: string;
  email: string;
}

const UserList = () => {
  const [users, setUsers] = useState<IUser[]>([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchUsers = async () => {
      try {
        const response = await fetch('https://jsonplaceholder.typicode.com/users');
        const data = await response.json();
        setUsers(data);
        setLoading(false);
      } catch (error) {
        console.error('Error fetching users:', error);
        setLoading(false);
      }
    };

    fetchUsers();
  }, []);

  return (
    <div>
      <h2>User List</h2>
      {loading ? (
        <p>Loading...</p>
      ) : (
        <ul>
          {users.map((user: IUser) => (
            <li key={user.id}>
              <strong>{user.name}</strong> - {user.email}
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};

export default UserList;
