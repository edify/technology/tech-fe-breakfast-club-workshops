# Tech FE Breakfast Club Workshops

* [2024-01-10] [Headless Component Pattern - Separation of concerns](./2024-01-10_workshop-headless-components-separation-of-concerns/README.md)
* [2023-11-29] [Compound Design Pattern to fix prop drilling](./2023-11-29_workshop-compound-pattern-prop-drilling/README.md)
* [2023-11-15] [Factory Design Pattern to fix excessive conditional rendering](./2023-11-15_workshop-factory-pattern-conditional-rendering/README.md)
