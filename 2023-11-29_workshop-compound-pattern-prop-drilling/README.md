# Breakfast Club Workshop: Factory Design Pattern

[Session Recording](https://drive.google.com/file/d/1q_wj-TxoOppuFlF3MhQycBqIB_2j2e4U/view)

Loosely based on:
1. [patterns.dev](https://www.patterns.dev/react/compound-pattern)
1. [React Design Patterns: Compound Component Pattern](https://medium.com/@vitorbritto/react-design-patterns-compound-component-pattern-ec247f491294)
1. [Compound Components In React](https://www.smashingmagazine.com/2021/08/compound-components-react/)

## What is Compount Pattern?

Compound Pattern is an specific application of [composite design pattern](https://refactoring.guru/design-patterns/composite), which is a structural design pattern that lets you compose objects into tree structures and then work with these structures as if they were individual objects.

[Compound components](https://medium.com/@vitorbritto/react-design-patterns-compound-component-pattern-ec247f491294) is a pattern for web component driven technologies, where several components are used together such that they share an implicit state that allows them to communicate with each other in the background.

This pattern is used when multiple components work together to have a shared state and handle logic together. It can also be usend in [Svelte](https://render.com/blog/svelte-design-patterns#component-composition) and [Angular](https://www.danywalls.com/how-to-build-composable-compound-components-in-angular#heading-what-is-compound-component).

Even HTML applies this pattern in some of its tags:

```html
<select name="frameworks" id="frameworks">
  <option value="angular">Angular</option>
  <option value="next">Next.js</option>
  <option value="nuxt">Nuxt.js</option>
  <option value="sveltekit">Svelte Kit</option>
</select>
```
A very good compound component example is the modal implementation in [react-boostrap](https://react-bootstrap.netlify.app/docs/components/modal). In this case the excessive rendering is avoided by composing modal component by embracing the use of `children` prop.

## Hands-on exercise

Fix the [ExampleWithPropDrilling](https://gitlab.com/edify/technology/tech-fe-breakfast-club-workshops/-/blob/main/2023-11-29_workshop-compound-pattern-prop-drilling/src/ExampleWithPropDrilling.tsx) implementation by fixing the **prop drilling code smell**. There're other things in that component that can and should be fixed, so feel free to change everything you think will improve the code: add/remove components, use `children` prop, create contexts, etc.


### In order to run the app
 ```
 npm install
 npm run dev
 ```

## Cool things to do

* **Provided example was too easy?** Ask ChatGPT or Bard to generate a complex example with prop drilling of `children` prop misuse an fixing using the concepts learned before.

* Do you think you have **something in your project that could be fixed** with the Compound/Composite Pattern? Let's check it out and maybe together we can improve your project.

* Why only Javascript/Typescript and React? Do you think this pattern can be applied in another programming language, framework or technology? **Create a merge request and add your own example**.
