import React, { createContext, useContext, useState, ReactNode } from 'react';


interface IContext {
  count?: number;
  increment?: () => void;
  decrement?: () => void;
}

// Step 1: Create a context to hold the state
const MyContext = createContext<IContext>({});

// Step 2: Create a parent component that will hold the state
const ParentComponent: React.FC<{ children: ReactNode }> = ({ children }) => {
  const [count, setCount] = useState(0);

  const increment = () => {
    setCount(count + 1);
  };

  const decrement = () => {
    setCount(count - 1);
  };

  // Step 3: Provide the state and functions to the context
  const contextValue = {
    count,
    increment,
    decrement,
  };

  return <MyContext.Provider value={contextValue}>{children}</MyContext.Provider>;
};

// Step 4: Create child components that use the context
const DisplayComponent = () => {
  const { count } = useContext(MyContext);

  return <div>Count: {count}</div>;
};

const IncrementButton = () => {
  const { increment } = useContext(MyContext);

  return <button onClick={increment}>Increment</button>;
};

const DecrementButton = () => {
  const { decrement } = useContext(MyContext);

  return <button onClick={decrement}>Decrement</button>;
};

// Step 5: Use the compound components in your app
const App = () => {
  return (
    <ParentComponent>
      <DisplayComponent />
    <IncrementButton />
    <DecrementButton />
    </ParentComponent>
  );
};

export default App;
