import React from 'react';


export interface Data {
  title: string;
  message: string;
}

const GrandparentComponent = () => {
  const data: Data = {
    title: 'Hello from GrandparentComponent!',
    message: 'This is a message from the GrandparentComponent.'
  };

  return (
    <div>
      <ParentComponent data={data} />
    </div>
  );
};

export interface DataProps {
  data: Data;
}

const ParentComponent: React.FC<DataProps> = ({ data }) => {
  return (
    <div>
      <h2>{data.title}</h2>
      <ChildComponent data={data} />
    </div>
  );
};

const ChildComponent: React.FC<DataProps> = ({ data }) => {
  return (
    <div>
      <p>{data.message}</p>
    </div>
  );
};

export default GrandparentComponent;
