import React from 'react';

import './App.css'
import ExampleWithPropDrilling from "./ExampleWithPropDrilling.tsx";
import CompoundExample from "./CompoundExample.tsx";

function App() {

  return (
    <>
      <h1>The Breakfast Club: Compound Components for prop drilling and avoiding excessive rendering </h1>
      <CompoundExample />
      <ExampleWithPropDrilling />
    </>
  )
}

export default App
