# Breakfast Club Workshop: Factory Design Pattern

[Session Recording](https://drive.google.com/file/d/161DaHXTa-oxe9OGXE6CD8-xb_j3LLAVL/view?usp=sharing)

_Loosely based on [patterns.dev](https://patterns.dev) and [react-design-patterns](https://github.com/themithy/react-design-patterns) examples_

The [factory design pattern in Java](https://www.digitalocean.com/community/tutorials/factory-design-pattern-in-java) is used when we have a superclass with multiple sub-classes and based on input, we need to return one of the sub-class. This pattern takes out the responsibility of the instantiation of a class from the client program to the factory class. Let’s first learn how to implement a factory design pattern in java and then we will look into factory pattern advantages. We will see some of the factory design pattern usage in JDK. Note that this pattern is also known as Factory Method Design Pattern.

In [Javascript](https://www.patterns.dev/vanilla/factory-pattern), you can also apply the **factory design pattern**

Even in [React Native](https://medium.com/mop-developers/factory-pattern-in-react-native-without-using-switch-df99bca31a55) you can apply the Factory Pattern.

The [Factory Design Pattern can be used in React](https://github.com/themithy/react-design-patterns/blob/master/doc/factory-pattern.md) to encapsulates the process of creating a component.

## Hands-on exercise

Fix the [ExampleWithoutFactory] implementation by fixing the **excessive conditional rendering code smell**. There're other things in that component that can and should be fixed, so feel free to change everything you think will improve the code: add components, remove components, fix the styling, avoid using literals, apply parametrized factory pattern, etc.

### In order to run the app
 ```
 npm install
 npm run dev
 ```

## Cool things to do
* Do you think you have something in your project that could be fixed with the Factory Pattern? Let's check it out and maybe together we can improve your project.

* Why only Javascript/Typescript and React? Do you think this pattern can be applied in another programming language, framework or technology? **Create a merge request and add your own example**.


## Advanced concepts and examples

* [React.js with Factory Pattern: Building Complex UI With Ease](https://blog.bitsrc.io/react-js-with-factory-pattern-building-complex-ui-with-ease-fe6db29ab1c1) $$$
* [Extendable Factory Pattern for React Using TypeScript](https://betterprogramming.pub/extendable-factory-pattern-for-react-using-typescript-3298c59fefd8): Create an extendable factory pattern to allow users of a module to extend your factory class
* **JavaScript Design Patterns Part 1: The Factory Pattern** [[Part 1](https://medium.com/@thebabscraig/javascript-design-patterns-part-1-the-factory-pattern-5f135e881192)] [[Part 2](https://medium.com/@thebabscraig/javascript-design-patterns-part-2-the-publisher-subscriber-pattern-8fe07e157213)]

