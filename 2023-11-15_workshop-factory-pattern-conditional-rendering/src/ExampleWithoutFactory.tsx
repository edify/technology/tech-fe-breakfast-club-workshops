import React, {FC} from 'react';



interface IMyComponent {
  shapeType: string;
}

const Shapes:FC<IMyComponent> = ({ shapeType }) => {
  return (
    <div>
      { shapeType === "circle" && (<div style={{ borderRadius: '50%', width: '100px', height: '100px', backgroundColor: 'blue' }} />) }
      { shapeType === "triangle" && (<div
        style={{
          width: '0',
          height: '0',
          borderLeft: '50px solid transparent',
          borderRight: '50px solid transparent',
          borderBottom: '87px solid red',
        }}
      />) }
      { shapeType === "square" && (<div style={{ width: '100px', height: '100px', backgroundColor: 'green' }} />) }
    </div>
  );
};

// Example usage
export const ExampleWithoutFactory = () => {
  return (
    <div>
      <Shapes shapeType="circle" />
      <Shapes shapeType="square" />
      <Shapes shapeType="triangle" />
    </div>
  );
};
