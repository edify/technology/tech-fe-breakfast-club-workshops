// Product interface
class Car {
  drive() {
    console.log('Default driving implementation');
  }
}

// Concrete products
class Sedan extends Car {
  drive() {
    console.log('Driving a sedan');
  }
}

class SUV extends Car {
  drive() {
    console.log('Driving an SUV');
  }
}

// Factory interface
class CarFactory {
  createCar() {
    throw new Error('createCar method must be implemented');
  }
}

// Concrete factories
class SedanFactory extends CarFactory {
  createCar() {
    return new Sedan();
  }
}

class SUVFactory extends CarFactory {
  createCar() {
    return new SUV();
  }
}

// Client code
const sedanFactory = new SedanFactory();
const suvFactory = new SUVFactory();

const mySedan = sedanFactory.createCar();
const mySUV = suvFactory.createCar();

mySedan.drive(); // Output: Driving a sedan
mySUV.drive();   // Output: Driving an SUV
