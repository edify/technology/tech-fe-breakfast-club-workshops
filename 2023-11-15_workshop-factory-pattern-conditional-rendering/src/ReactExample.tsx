import React from 'react';

// Product interface
function Button(): React.ReactComponentElement<any> {
    return <button>Default Button</button>;
}

// Concrete products
function PrimaryButton(): React.ReactComponentElement<any> {
    return <button style={{ backgroundColor: 'blue', color: 'white' }}>Primary Button</button>;
}

function SecondaryButton(): React.ReactComponentElement<any> {
    return <button style={{ backgroundColor: 'gray', color: 'black' }}>Secondary Button</button>;
}

// Factory method
class ButtonFactory {
  createButton() {
    throw new Error('createButton method must be implemented');
  }
}

// Concrete factories
class PrimaryButtonFactory extends ButtonFactory {
  createButton() {
    return <PrimaryButton />;
  }
}

class SecondaryButtonFactory extends ButtonFactory {
  createButton() {
    return <SecondaryButton />;
  }
}

// Client code (React component)
export function Example(): React.ReactComponentElement<any> {
  const primaryButtonFactory = new PrimaryButtonFactory();
  const secondaryButtonFactory = new SecondaryButtonFactory();

  const primaryButton = primaryButtonFactory.createButton();
  const secondaryButton = secondaryButtonFactory.createButton();

  return (
    <div>
      {primaryButton}
      {secondaryButton}
    </div>
  );
};

