import React from 'react';

import './App.css'
import {Example} from "./ReactExample.tsx";
import {ExampleWithoutFactory} from "./ExampleWithoutFactory.tsx";

function App() {

  return (
    <>
      <h1>The Breakfast Club: Factory Dessign Pattern To Fix Excessive Conditional Rendering Code Smells </h1>
      <Example />
      <ExampleWithoutFactory />
    </>
  )
}

export default App
